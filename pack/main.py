import ldap
from flask import Blueprint, request, render_template
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, StringField
from wtforms.validators import DataRequired

ldap_print = Blueprint('ldap_print', __name__)


class LdapForm(FlaskForm):
    firstname = StringField('Vorname:', validators=[DataRequired()])
    lastname = StringField('Nachname:', validators=[DataRequired()])
    old_pass = PasswordField('Altes Passwort:', validators=[DataRequired()])
    new_pass = PasswordField('Neues Passwort:', validators=[DataRequired()])
    new_pass_repeat = PasswordField('Passwort wiederholen:', validators=[DataRequired()])
    submit = SubmitField('Passwort ändern')


class LdapForm1(FlaskForm):
    cn = StringField('Vor- und Nachname', validators=[DataRequired()])
    gidNumber = StringField('gidNummer: (immer 1000)', validators=[DataRequired()])
    homeDirec = StringField('Home Directory (zb. /home/mwilka)', validators=[DataRequired()])
    sn = StringField('Nachname', validators=[DataRequired()])
    uid = StringField('Erster Buchstabe (Vorname) und Nachname (zb. mwilka)', validators=[DataRequired()])
    uidNumber = StringField('Eine Nummer (Letzte: 1021)', validators=[DataRequired()])
    mail = StringField('E-Mail Addresse', validators=[DataRequired()])
    userPassword = PasswordField('Passwort', validators=[DataRequired()])
    submit = SubmitField('Neuen User erstellen')


@ldap_print.route('/ldap')
def ldap_app():
    form = LdapForm()
    return render_template('form.html', title='Change ldap password', form=form)

@ldap_print.route('/ldap/create-user')
def create_ldap_user():
    form = LdapForm1()
    return render_template('create-user-form.html', form=form)


@ldap_print.route('/handle_data', methods=['POST'])
def handle_data():
    fname = request.form['firstname']
    lname = request.form['lastname']
    opass = request.form['old_pass']
    npass = request.form['new_pass']
    nrpass = request.form['new_pass_repeat']

    # change ldap pass here
    try:
        l = ldap.initialize('ldap://services.deinsportplatz.de')
        ldap_string = 'cn=' + fname + ' ' + lname + ',ou=users,dc=sportplatz-media,dc=com'

        l.simple_bind_s(str(ldap_string), str(opass))
        if npass == nrpass:
            m = 'Success'
            l.passwd_s(ldap_string, opass, npass)
        else:
            m = 'Passwords doesnt match'
    except Exception as e:
        m = str(e)

    # response
    return render_template('response.html', msg=m)


@ldap_print.route('/handle_create_user')
def handle_create_user():
    cn = request.form['cn']
    gidNumber = request.form['gidNumber']
    homeDirec = request.form['homeDirec']
    sn = request.form['sn']
    uid = request.form['uid']
    uidNumber = request.form['uidNumber']
    mail = request.form['mail']
    userPassword = request.form['userPassword']

    # create new cn here to group users
    try:
        con = ldap.initialize('ldap://services.deinsportplatz.de')
        dn = 'uid=' + uid + "ou=users,dc=sportplatz-media,dc=com"

        modlist = {
            'objectClass': ['inetOrgPerson', 'organizationalPerson', 'person', 'posixAccount', 'top'],
            'cn': [str(cn)],
            'gidNumber': [str(gidNumber)],
            'homeDirectory': [str(homeDirec)],
            'sn': [str(sn)],
            'uid': [str(uid)],
            'uidNumber': [str(uidNumber)],
            'mail': [str(mail)],
            'userPassword': [str(userPassword)]
        }

        # insert here
        con.add_s(dn, ldap.modlist.addModlist(modlist))
        m = 'Neuer User erstellt.'

    except Exception as e:
        m = str(e)

    return render_template('response.html', msg=m)
