import yaml

path = '/Users/bjarne/PycharmProjects/ldap/app_config.yml'

with open(path, 'r') as config_file:
    global app_conf
    app_conf = yaml.load(config_file)
