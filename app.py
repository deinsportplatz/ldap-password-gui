import os
from flask import Flask, render_template
from waitress import serve
from config_reader import app_conf
from pack.main import ldap_print

SECRET_KEY = os.urandom(32)

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY

app.register_blueprint(ldap_print)


def main():
    with app.app_context():
        serve(app, host=app_conf['http_host'], port=app_conf['http_port'])


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


if __name__ == '__main__':
    main()
